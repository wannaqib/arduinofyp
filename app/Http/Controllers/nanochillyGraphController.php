<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class nanochillyGraphController extends Controller
{
    function search(Request $request){
        $input = $request->input("searchDate");
        return redirect('graphnc?date=' . $input);
    }
}
