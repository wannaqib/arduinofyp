@extends('layouts.navbar')
@section('script')


<div class="row">
    <div id="login_div" style="margin-top: 20px;margin-left:50px; font-size:20px">
        <p>You need to log in first !</p>

    </div>
</div>

<div class="row">
    <div class="container-fluid" style="max-width:800;min-width:200;height:400px;margin-top:5%">
        <!-- calender-->
        <div id="calender" style="margin-left:20%">
            <form action="/searchDate" method="POST">
                @csrf
                <label for="datepicker">View Temperature Based on Date:</label>
                <input type="date" id="datepicker" name="searchDate">
                <button type="submit" class="btn" style="height:39px; margin-bottom:1%">Submit</button>
            </form>
            <!-- 
                1. kita search based on date chose
                2. dia route ke controller(searchDate)
                3. dia akan return link with input
                -->
            <div>
                <H1 id="dateChose" style="margin-left:150px"></H1>
            </div>

        </div>

        <div class="row" id="logged_div"
            style="text-align:center;max-width:900px;max-height:400px;margin-left:0%;margin-bottom:30%">
            <script>
                var chart1;
                var roomHumid;
                var roomTemp;
                var waterTemp;
                var hello;
                var timeList = [];
                var roomTempList = [];
                var waterTempList = [];
                var monthList = [
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "May",
                    "June",
                    "Jul",
                    "Aug",
                    "Sep",
                    "Oct",
                    "Nov",
                    "Dec"
                ]


                var getParams = function (url) {
                    var params = {};
                    var parser = document.createElement('a');
                    parser.href = url;
                    var query = parser.search.substring(1);
                    var vars = query.split('&');
                    for (var i = 0; i < vars.length; i++) {
                        var pair = vars[i].split('=');
                        params[pair[0]] = decodeURIComponent(pair[1]);
                    }
                    return params;
                };
                //how to take date from the input on the php form on the URL
                //extract 

                let dateString = getParams(window.location.href)
                    .date; //this will hold the value of date from the url extracted

                if (dateString) {
                    document.getElementById("dateChose").textContent = dateString;
                    // we slice the string to get each of the character
                    var month = Number.parseInt(dateString.slice(5, 7)) - 1;
                    //why we tolak 1 sebb array of month untuk nombor 6 is july, so tolak 1 jadi june
                    // let month = dateString.slice(5,7);
                    // if(month[0]=="0"){
                    //     month=month[1];
                    // }

                    // todo: parse the day from dateString
                    var day = Number.parseInt(dateString.slice(8));
                    var year = Number.parseInt(dateString.slice(0, 4));

                    var minBound = Date.parse(
                        `${day} ${monthList[month]} ${year} 00:00:00 GMT`
                        ); // this code will generate min time of that date
                    var maxBound = Date.parse(
                        `${day} ${monthList[month]} ${year} 23:59:59 GMT`
                        ); //this code will generate max time of that date
                    //this code will be used to compare the time in millisecond, like epochtime
                }

                ref30.on('value', function (snapshot) { //here we are just calling all the data from the firebase
                        var value = snapshot.val();

                        for (let key in value) {
                            let date = new Date((value[key].date) * 1000)
                            //minBound and maxBound

                            // todo: template string -> pass the month and day variables to Date.parse
                            if (!dateString) {
                                let time = value[key].Time;
                                let roomTemp = value[key].Room_Temp;
                                let waterTemp = value[key].Water_Temp;
                                roomTempList.push(roomTemp);
                                waterTempList.push(waterTemp);
                                date.setHours(date.getHours() - 8);
                                date =
                                    `${date.getDate()}/${monthList[date.getMonth()]} ${date.getHours()}:${date.getMinutes()}`
                                timeList.push(date);
                                //it will display all data when datastring is null
                            } else {
                                const withinBound = (date >= minBound && date <= maxBound)

                                if (withinBound) {
                                    let time = value[key].Time;
                                    let roomTemp = value[key].Room_Temp;
                                    let waterTemp = value[key].Water_Temp;
                                    roomTempList.push(roomTemp);
                                    waterTempList.push(waterTemp);
                                    date.setHours(date.getHours() - 8); //for gmt purposes, epoch using +0
                                    date =
                                        `${date.getDate()}/${monthList[date.getMonth()]} ${date.getHours()}:${date.getMinutes()}`
                                    timeList.push(date);
                                    //it will show date string based on minbound and maxbound

                                }

                            }
                        }

                        if (firebase.auth().currentUser) {
                            Highcharts.chart('logged_div', {
                                    chart: {
                                        type: 'line',
                                    } ,
                                    title: {
                                        text: 'Room Temperature VS Water Temperature'
                                    },
                                    xAxis: {
                                        categories: timeList
                                    },
                                    yAxis: {
                                        title: {
                                            text: 'Temperature'
                                        }
                                    },
                                    series: [{
                                            name: 'Water Temperature',
                                            data: waterTempList
                                        }

                                        , {
                                            name: 'Room Temperature',
                                            data: roomTempList
                                        }

                                    ],
                                    responsive: {
                                        rules: [{
                                                condition: {
                                                    maxWidth: 500
                                                }

                                                ,
                                                chartOptions: {
                                                    legend: {
                                                        layout: 'horizontal',
                                                        align: 'center',
                                                        verticalAlign: 'center'
                                                    }
                                                }

                                            }

                                        ]
                                    }
                                }

                            );

                        }


                    }

                );

            </script>
        </div>


    </div>
</div>







@endsection
