<html>

<head>
    {{-- js script --}}


    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="{{ asset('cssFile.css') }}">
    <script src="https://code.highcharts.com/highcharts.js"></script>

    <title>NanoChilly</title>
</head>

<body class="container-fluid" style="overflow-y:hidden">

    <div style="postion:fluid;top:0px;width:100%">
        <nav class="navbar navbar-expand-lg navbar-light " style="height:45px">
            <a class="navbar-brand" style="" href="/homenc">NanoChilly</a>
            <button style="margin-left:80%" class="navbar-toggler" type="button" data-toggle="collapse"
                data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav" style="position:relative">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <a class="nav-link" style="" href="/homenc">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="/graphnc">Graph</a>
                    </li>
                    <li id="login_butt" class=" nav-item active" style="margin-right:30%;">
                        <a class=" nav-link" style=" width:100px" href="/loginc">Log in</a>
                    </li>
                </ul>
                <button id="logout_butt" class="logout-div " style="position:absolute; right:0px;margin-top:5px"
                    onclick="logout()">Log Out</button>
            </div>
            
         

        </nav>
    </div>

    <div>
        @yield('content')
    </div>


    <script src="https://code.jquery.com/jquery-3.4.0.min.js"></script>
    <script src="https://www.gstatic.com/firebasejs/5.10.1/firebase.js"></script>
    <script>
        var config = {
            apiKey: "AIzaSyDq2XH7euYeIDMhCxjRnXIlPiJgSKfDbwU",
            authDomain: "nanochilly.firebaseapp.com",
            databaseURL: "https://nanochilly.firebaseio.com",
            storageBucket: "nanochilly.appspot.com",
        };

        firebase.initializeApp(config);

        var database = firebase.database();
        var ref = database.ref('nanochilly/Temperature/');
        var refSend = database.ref('nanochilly/IO');
        var s1 = database.ref('nanochilly/IO');
        var s0 =database.ref('nanochilly/IO');
        var ref30 = database.ref('nanochilly/Temperature30/');
        var limRefSend = database.ref('nanochilly/limit');
        var refInterval = database.ref('nanochilly/interval');
        var lastIndex = 0;
        var rTemp30; // link javascript to html
        var wTemp30;
        var rHumid30;
        var time;


        firebase.auth().onAuthStateChanged(function (user) {
            if (user) {

                document.getElementById("login_div").style.display = "none";
                document.getElementById("login_butt").style.display = "none";
                document.getElementById("logout_butt").style.display = "block";
                document.getElementById("logged_div").style.display = "block";
                document.getElementById("calender").style.display = "block";
                
            } else {
                
                document.getElementById("login_div").style.display = "block";
                document.getElementById("login_butt").style.display = "block";
                document.getElementById("logout_butt").style.display = "none";
                document.getElementById("logged_div").style.display = "none";
                document.getElementById("calender").style.display = "none"; 

            }
        }); //in this part, firebase will allow user to see what they should and what they shouldnt


        function login() {
            var userEmail = document.getElementById("emailUser").value;
            var passUser = document.getElementById("passUser").value;

            // alert(userEmail + "naqib cantik" + passUser);
            firebase.auth().signInWithEmailAndPassword(userEmail, passUser).then(function () {
                window.location.href = "/homenc";
            }).catch(function (error) {
                    // Handle Errors here.
                    var errorCode = error.code;
                    var errorMessage = error.message;
                    window.alert("Error: " + errorMessage);
                }

            ); //this is how the password and email is processed by using this password!
            // window.location.href="/homeNC";

        } //this is the log in function that will be called upon clicking the login in button

        function signup() {
            var signupUser = document.getElementById("signupUser").value;
            var signpassUser = document.getElementById("signpassUser").value;

            firebase.auth().createUserWithEmailAndPassword(signupUser, signpassUser).catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                if (errorCode == 'auth/weak-password') {
                    alert('The password is too weak.');
                } else {
                    alert(errorMessage);
                }
                console.log(error);
                // [END_EXCLUDE]

                // ...
            }).then(function () {
                alert("You're Signed Up");
                window.location.href = "/homenc";
            });
        }

        function logout() {
            firebase.auth().signOut().then(function () {
                window.location.href = "/loginc";
                // Sign-out successful.
            }).catch(function (error) {
                // An error happened.
            });
        }

    </script>

   
        @yield('script')

    

</body>

</html>
