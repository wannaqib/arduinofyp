@extends('layouts.navbar')
@section('content')
<div class="row Par-loginrow" style="height:100%;z-index:-10">

    <div class="col-md-9" id="" style="padding:0px;">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="margin-top:100px">
            <ol class="carousel-indicators">
                <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{ asset('FullSizeRender.jpg') }}"
                        alt="First slide">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{ asset('coffee.jpg') }}" alt="Second slide"
                        style="height:656px">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="{{asset('redred.jpg')}}" alt="Third slide" style="height:656px">
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <!-- <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel" style="margin-top:100px">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="{{ asset('FullSizeRender.jpg') }}"
                        alt="First slide">
                </div>
                <div class="carousel-item ">
                    <img class="d-block w-100" src="{{ asset('coffee.jpg') }}" alt="Second slide" style="height:650px">
                </div>
                <div class="carousel-item">
                    <img class="d-block w-100" src="..." alt="Third slide">
                </div> -->
        <!-- </div>
        </div> 
        < <img class="img-fluid" src="{{ asset('FullSizeRender.jpg') }}" alt="" style="height:100%;"> -->
    </div>

    <div class="col-md-3 login-row login-div " id="login_div" style="text-align:left;padding:20px">
        <h2>SIGN IN</h2>
        <input type="email" placeholder="Email..." id="emailUser">
        <input type="password" placeholder="Password..." id="passUser">
        <button onclick="login()">Login</button>


        <h2 style="margin-top:20px;">NEW USER SIGN UP</h2>
        <input type="email" placeholder="Email..." id="signupUser">
        <input type="password" placeholder="Password..." id="signpassUser">
        <button onclick="signup()">Sign Up</button>
    </div>


</div>


@endsection

@section('script')
<script>

</script>
@endsection
