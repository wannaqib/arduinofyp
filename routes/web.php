<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/nanoChilly','FirebaseController@index');

Route::get('/homenc',function(){
    return view('pages.nanochillyMainPage');
});

Route::get('/graphnc',function(){
    return view('pages.nanochillyGraph');
});

Route::get('/loginc',function(){
    return view('pages.logInPage');
});

Route::get('/nb',function(){
    return view('layouts.navbar');
});

Route::POST('/searchDate', 'nanochillyGraphController@search');