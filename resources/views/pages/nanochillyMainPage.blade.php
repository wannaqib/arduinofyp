@extends('layouts.navbar')
@section('content')
<div>
    <div id="login_div" style="margin-top: 20px;margin-left:50px; font-size:20px">
        You need to log in First!
    </div>

    <div id="logged_div">
        <div class="card besar">
            <div class="card-deck text-center" style="margin: auto 28%;margin-top:2% ">
                <div class="card kecik" style="margin: 1%">
                    <div class="card-body">
                        <h5 class="card-title">Room Temperature</h5>
                        <p class="card-text" id="rTemp"></p>
                    </div>
                </div>
                <div class="card kecik" style="margin: 1%">
                    <div class="card-body">
                        <h5 class="card-title">Room Humidity</h5>
                        <p class="card-text" id="rHumid"></p>
                    </div>
                </div>
                <div class="card kecik" style="margin: 1%">
                    <div class="card-body">
                        <h5 class="card-title">Water Temperature</h5>
                        <p class="card-text" id="wTemp"></p>

                    </div>
                </div>

                <div class="card" style="margin:1%;border-width:2px">
                    <div>
                        <h5 style="margin-top:20px">Fan Control</h5>
                        <!-- <div>
                            <div id="ioVal"></div>
                        </div> -->
                        <button id="on" class="btn" value="1" style="margin-top:35px;margin-bottom:2%"
                            onclick="arduinoON()">
                            On</button>
                        <button id="off" class="btn" value="0" style="margin-top:35px;margin-bottom:2%"
                            onclick="arduinoOff()">
                            Off</button>
                    </div>
                </div>

                <div class="card" style="margin:1%;border-width:2px">
                    <div>
                        <h5 style="margin-top:20px">Temperature Reading Interval</h5>
                        <!-- <div>
                            <div id="intval"></div>
                        </div> -->
                        <button id="int30" class="btn" value="30" style="margin-top:20px;margin-bottom:2%"
                            onclick="interval30()">30
                            mins
                        </button>
                        <button id="int60" class="btn" value="60" style="margin-top:20px;margin-bottom:2%"
                            onclick="interval60()">1
                            hr</button>
                    </div>

                </div>
            </div>




            <!-- <div>

                <div class="container-fluid text-center" style="margin-top: 3%">

                    <div class="row text-center" style="margin-bottom:3%">
                        <p class="text-center" style=" margin:auto; font-size:30px">Temperature Limit: </p>
                    </div>

                    <div class="row text-center" style="margin-bottom:3%">
                        <div style="width:50%;margin:auto">
                            <input type="range" min="0" max="50" id="slider"
                                style="width:50%;margin-top:5%;trackColor:#FFCB2B">
                            <span id="slideVal" style=""></span>
                        </div>
                    </div>
                </div>
            </div> -->
        </div>

        <!-- <div id="logout_butt" class="logout-div ">
            <button class="logout-div" style="margin-top:3%" onclick="logout()">Log Out</button>
        </div> -->
    </div>

</div>
<div id="calender"></div>


@endsection

@section('script')
<script>
    // var slideElement = document.getElementById("slider");
    // var slideVal = document.getElementById("slideVal");

    // slideElement.value = limRefSend.on('value', function (snapshot) {
    //     var value = snapshot.val();
    //     slideElement.value = value.lim;
    //     slideVal.innerText = slideElement.value;
    // });




    function toggleButtonColour(e) {
        console.log(e);
        console.log("hello")
    }

    // slideElement.oninput = function () {
    //     slideVal.innerText = slideElement.value;
    //     limRefSend.set({
    //         lim: slideVal.innerText
    //     })

    // }

    function formatDecimal(num, decimals) {
        let dec = Math.pow(10, 2);
        return Math.round(num * dec) / dec;
    }
    
    var rTemp = document.getElementById("rTemp"); // link javascript to html
    var wTemp = document.getElementById("wTemp");
    var rHumid = document.getElementById("rHumid");
    ref.on('value', function (snapshot) {
        var value = snapshot.val();
        console.log(value); //this is the function to call value from
        rHumid.innerText = value.Room_Humidity + "%"; //after link, assign kpada id dekat mainpage
        wTemp.innerText = formatDecimal(value.Water_Temp, 1) + "°C";
        if (wTemp.innerText >= '30') {
            send1();
        } else {
            send0();
        }
        rTemp.innerText = value.Room_Temp + "°C";
    });

    var ioVal = document.getElementById("ioVal"); //taking value from database
    refSend.on('value', function (snapshot) {
        var value = snapshot.val();
        if (value.IO) {
            document.getElementById("on").classList.add("toggle")
            document.getElementById("off").classList.remove("toggle")
        } else {
            document.getElementById("off").classList.add("toggle")
            document.getElementById("on").classList.remove("toggle")
        }
        //ioVal.innerText = value.IO;

    })

    var intval = document.getElementById("intval"); //taking value from database
    refInterval.on('value', function (snapshot) {
        var value = snapshot.val();
        if (value.Interval == 30) {
            document.getElementById("int30").classList.add("toggle")
            document.getElementById("int60").classList.remove("toggle")
        } else {
            document.getElementById("int60").classList.add("toggle")
            document.getElementById("int30").classList.remove("toggle")
        }
        //intval.innerText = value.Interval;

    })

    function arduinoON() {
        refSend.set({
            IO: parseInt(document.getElementById("on").value)
        });
    }

    function arduinoOff() {
        refSend.set({
            IO: parseInt(document.getElementById("off").value)
        });
    }

    function interval30() {
        refInterval.set({
            Interval: parseInt(document.getElementById("int30").value)
        });

    }

    function interval60() {
        refInterval.set({
            Interval: parseInt(document.getElementById("int60").value)
        });

    }

    function send1() {
        alert("hantar 1");
        s1.set({
            IO:1
        });
    }

    function send0() {
        alert("hantar 0");
        s0.set({
            IO:0
        });
    }

</script>
@endsection
